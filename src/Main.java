import java.util.ArrayList;
import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class Main {

    private static double a = 2.4;
    private static double b = 1.8;
    private static double c = 2.9;

    public static void main(String[] args) {

        double[] xBasis = { 1 , 1 };

        int n = 2;

        double m = 0.25;

        double E = 0.1;

        double sigma1, sigma2;

        sigma1 = ( (sqrt(n + 1) - 1) / (n * sqrt(2)) ) * m;

        sigma2 = ( (sqrt(n + 1) + n - 1) / (n * sqrt(2)) ) * m;

        ArrayList<double[]> xVectors = new ArrayList<>();

        ArrayList<double[]> xPoints = new ArrayList<>();

        int i = 0;
        xVectors.add(new double[]{i, xBasis[0], xBasis[1], function(xBasis[0], xBasis[1])});
        xPoints.add(xVectors.get(0));

        i++;
        xVectors.add(new double[]{i, sigma1 + xBasis[0], sigma2 + xBasis[1], function(sigma1 + xBasis[0], sigma2 + xBasis[1])});
        xPoints.add(xVectors.get(1));

        i++;
        xVectors.add(new double[]{i, sigma2 + xBasis[0], sigma1 + xBasis[1], function(sigma2 + xBasis[0], sigma1 + xBasis[1])});
        xPoints.add(xVectors.get(2));

        double[] coordOfCenter = {1, 1};

        do {

            coordOfCenter[0] = 0;
            coordOfCenter[1] = 0;

            i++;

            int indexOfMax = findBiggest(xVectors);

            double[] tmp = xVectors.remove(indexOfMax);

            for (double[] list : xVectors) {
                coordOfCenter[0] = coordOfCenter[0] + list[1];
                coordOfCenter[1] = coordOfCenter[1] + list[2];
            }

            double[] newPoint = {i, coordOfCenter[0] - tmp[1], coordOfCenter[1] - tmp[2], function(coordOfCenter[0] - tmp[1], coordOfCenter[1] - tmp[2])};

            xVectors.add(newPoint);
            xPoints.add(newPoint);

            if(newPoint[3] > tmp[3]){ //reduction
                reduction(xVectors);
            }

            coordOfCenter[0] = 0;
            coordOfCenter[1] = 0;

            for (double[] list : xVectors) {
                coordOfCenter[0] = coordOfCenter[0] + list[1];
                coordOfCenter[1] = coordOfCenter[1] + list[2];
            }

            coordOfCenter[0] = coordOfCenter[0] / 3;
            coordOfCenter[1] = coordOfCenter[1] / 3;


        }while (!checkIteration(xVectors,coordOfCenter, E));
        show(xVectors);
        showRes(xPoints);
    }

    public static double function(double x1, double x2){ return a * x1 * x1 + b * x2 * x2 - c * x2; }

    public static void reduction ( ArrayList<double[]> xVectors ){

        int indexOfSmallest = findSmaller(xVectors);

        double[] smallPoint = xVectors.get(indexOfSmallest);

        for  (double [] point : xVectors){
            if (point[2] != smallPoint[2] && point[1] != smallPoint[1]){
                point[1] = smallPoint[1] + 0.5 * (point[1] - smallPoint[1]);
                point[2] = smallPoint[2] + 0.5 * (point[2] - smallPoint[2]);
                point[3] = function(point[1], point[2]);
            }
        }
    }

    public static int findSmaller(ArrayList<double[]> funcs){

        double smallest = funcs.get(0)[3];

        int i = 0, j = 0;

        for (double[] f : funcs){

            if (f[3] < smallest){
                smallest = f[3];
                j = i;
            }
            i++;
        }

        return j;
    }

    public static int findBiggest(ArrayList<double[]> funcs){

        double biggest = funcs.get(0)[3];

        int i = 0, j = 0;

        for (double[] f : funcs){

            if (f[3] > biggest){
                biggest = f[3];
                j = i;
            }
            i++;
        }
        return j;
    }

    public static boolean checkIteration(ArrayList<double[]> currentFunctions, double[] centreOfGravity, double E){

        double funcCentr = function(centreOfGravity[0], centreOfGravity[1]);

        for (double[] func : currentFunctions){
            if (abs(func[3] - funcCentr) > E){
                return false;
            }

        }
        return true;
    }


    public static void show(ArrayList<double[]> list){

        System.out.println("\nBasis:");
        for ( double[] value : list)
            System.out.println("x" + value[0] + ": {" + value[1] + ";" + value[2] + "} function: " + value[3]);

    }

    public static void showRes(ArrayList<double[]> list){

        System.out.println("\nAll vectors:");
        for ( double[] value : list)
            System.out.println("x" + value[0] + ": {" + value[1] + ";" + value[2] + "} function: " + value[3]);
    }
}
